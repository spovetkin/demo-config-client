package ru.spovetkin.demo.democonfigclient.model;

public class CurrentConfigurationModel {

    private String a;

    public CurrentConfigurationModel(String a) {
        this.a = a;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }
}
