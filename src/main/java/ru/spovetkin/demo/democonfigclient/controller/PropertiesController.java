package ru.spovetkin.demo.democonfigclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.spovetkin.demo.democonfigclient.model.CurrentConfigurationModel;

@RefreshScope
@RestController
public class PropertiesController {

    @Value("${a}")
    private String a;

    @RequestMapping("/properties")
    public CurrentConfigurationModel getCurrentConfiguration() {
        return new CurrentConfigurationModel(a);
    }

}
